
// Nav change with scroll
var header = $("#transparent");
$(window).scroll(function() {
	var scroll = $(window).scrollTop();
	if (scroll <= 0) {
		header.addClass("nav-transparent");
		header.removeClass("nav-colored");

	} else {
		header.removeClass("nav-transparent");
		header.addClass("nav-colored");
	}
});

function initialize() {
	//SideNav
	if (window.jQuery) {
		$(document).ready(function() {
			$(".sidebarNavigation .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu").show();
			$("body").append("<div class='overlay'></div>");
			$(".navbar-toggle, .navbar-toggler").on("click", function() {
				$(".sideMenu").addClass($(".sidebarNavigation").attr("data-sidebarClass"));
				$(".sideMenu, .overlay").toggleClass("open");
				$(".overlay").on("click", function() {
					$(this).removeClass("open");
					$(".sideMenu").removeClass("open")
				})
			});
			$("body").on("click", ".sideMenu.open .nav-item", function() {
				if (!$(this).hasClass("dropdown")) {
					$(".sideMenu, .overlay").toggleClass("open")
				}
			});
			$(window).resize(function() {
				if ($(".navbar-toggler").is(":hidden")) {
					$(".sideMenu, .overlay").hide()
				} else {
					$(".sideMenu, .overlay").show()
				}
			})
		})
	}

	// Slick Slider
	$('.slider-mobile-evento').slick({
		slidesToShow: 1.25,
		slidesToScroll: 0.875,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
	});
	$('.slider-mobile-home').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
	});
	$('.slider-mobile-home2').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
    });
    $('.banner-calendar').slick({
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        speed: 2000,
        arrows: false,
        infinite: true,
        slidesToShow: 1,
        dots: false,
        draggable: false
	});

	$('.slider1').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
	});
	$('.slider2').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
	});
	$('.slider3').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
	});
	$('.slider4').slick({
		slidesToShow: 1.25,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		mobileFirst: true,
		arrows: false,
		centerMode: false,
		infinite: false,
		responsive:
			[{
				breakpoint: 767,
				settings: "unslick"
			}]
    });
	$('.add-marks').slick({
		autoplay: false,
		// autoplaySpeed: 2000,
		// speed: 2000,
		fade: true,
		arrows: false,
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		centerMode: true,
		dots: true,
		draggable: true,
		responsive:
		[{
            breakpoint: 767,
            settings:{
                slidesToShow: 1.5,
                dots: false,
                fade: false
            }
		}]
	});
}
window.onload = initialize;

// Material Select Initialization
$(document).ready(function() {
	$('.mdb-select').materialSelect();
});


// Dinamic placeholder
if ($(window).width() < 768) {
    $("#tickeagorasearch").attr("placeholder", "Busque...");
}
else {
    $("#tickeagorasearch").attr("placeholder", "Busque por nome, cidade, categoria...");
}


// Back Button
$("#back2").click(function () {
    $("#scales").prop('checked', false);
    $(".editar-dados").hide();
    if ($('#scales').is(":checked")) {
        $(".editar-dados").fadeIn("slow");
        $("#tabs-panel-menu").fadeOut("slow");
        $(".tab-content").fadeOut("slow");
    } else {
        $(".editar-dados").fadeOut("slow");
        $("#tabs-panel-menu").fadeIn("slow");
        $(".tab-content").fadeIn("slow");
    }
});


// Painel Change
function valueChangedCheckPainel() {
    $(".editar-dados").hide();
    if ($('#scales').is(":checked")) {
        $(".editar-dados").fadeIn("slow");
        $("#tabs-panel-menu").fadeOut("slow");
        $(".tab-content").fadeOut("slow");
    } else {
        $(".editar-dados").fadeOut("slow");
        $("#tabs-panel-menu").fadeIn("slow");
        $(".tab-content").fadeIn("slow");
    }
}
function pencilDisable(){
	$(".editar-dados").fadeIn("slow");
    $("#tabs-panel-menu").fadeOut("slow");
	$(".tab-content").fadeOut("slow");
	$(".overlay").removeClass("open");
	$(".sideMenu").removeClass("open");
}    

$('.show-repair').click(function() {
    if ($('.dropdown-menu.dropdown-menu-right.shadow').hasClass('show')) {
		$('.dropdown-menu.dropdown-menu-right.shadow')
			.removeClass('show')
			.removeClass('animated')
			.removeClass('fadeOut');

			
		$('dropdown.d-sm-block.d-md-none')
			.removeClass('dropdown-animating');
    }
});

